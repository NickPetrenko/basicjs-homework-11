//2. Додати новий елемент форми із атрибутами:
//Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.

document.addEventListener("DOMContentLoaded", function() {
    var contentSection = document.getElementById('content');
    var footer = document.querySelector('footer');
  
    var createButton = document.createElement('button');
    createButton.setAttribute('id', 'btn-input-create');
    createButton.textContent = 'Create Input';
  
    contentSection.insertBefore(createButton, footer);
  
    createButton.addEventListener('click', function() {
      var input = document.createElement('input');
      input.setAttribute('type', 'text');
      input.setAttribute('placeholder', 'Enter text');
      input.setAttribute('name', 'input-name');
  
      contentSection.insertBefore(input, footer);
    });
});