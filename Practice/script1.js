//1. Додати новий абзац по кліку на кнопку.

document.addEventListener("DOMContentLoaded", function() {
    var button = document.getElementById('btn-click');
    var contentSection = document.getElementById('content');
  
    button.addEventListener('click', function() {
      var paragraph = document.createElement('p');
      paragraph.textContent = 'New Paragraph';
  
      contentSection.appendChild(paragraph);
    });
});